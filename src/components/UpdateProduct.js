import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Form, Button } from 'react-bootstrap';

export default function UpdateProduct() {
  const { user } = useContext(UserContext);
  const { productId } = useParams();

  const navigate = useNavigate();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (name || description || price) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price]);

  function updateProduct(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.isAdmin === true) {
          const requestBody = {};
          if (name) {
            requestBody.name = name;
          }
          if (description) {
            requestBody.description = description;
          }
          if (price) {
            requestBody.price = price;
          }
          fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify(requestBody),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data === true) {
                Swal.fire({
                  title: 'Product Updated',
                  icon: 'success',
                  text: 'Product Updated successfully!',
                });

                navigate("/admin-page");

              } else {
                Swal.fire({
                  title: 'Something went wrong',
                  icon: 'error',
                  text: 'Please try again.',
                });
              }
            })
            .catch((error) => {
              console.error('Error:', error);
            })
            .finally(() => {
              setName('');
              setDescription('');
              setPrice('');
            });
        } else {
          Swal.fire({
            title: 'Unauthorized Access',
            icon: 'error',
            text: 'You are not authorized to access this page.',
          });
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }

  if (user.isAdmin === true) {
    return (

      <Form onSubmit={(e) => updateProduct(e)}>
       <h1>Update Product</h1>
        <Form.Group className="my-3" controlId="name">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            type="text"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
            placeholder="Update Product Name"
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="description">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
            placeholder="Update Description"
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="price">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            value={price}
            onChange={(e) => {
              setPrice(e.target.value);
            }}
            placeholder="Update Price"
          />
        </Form.Group>

      {isActive ?
        <Button variant="primary" type="submit" id="submitBtn">
          Update Product
        </Button>
        :
        <Button variant="dark" type="button" id="submitBtn" disabled>
              Update Product
            </Button>
        }
      </Form>
    )
};
};