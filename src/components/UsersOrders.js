import { useState, useEffect } from 'react';
import { Table, Container, Row, Col } from 'react-bootstrap';

export default function Orders() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/allOrders`)
      .then((res) => res.json())
      .then((data) => {
        if (Array.isArray(data)) {
          setOrders(data);
        } else {
          console.error('Error: API response is not an array');
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }, []);

  return (
    <Container className="text-center">
      <Row>
        <Col className="p-5">
          <h1>Hello Admin!</h1>
          <h3>List of Orders</h3>
        </Col>
      </Row>
      <Row className="justify-content-center align-items-center">
        <Col>
          <Table>
            <thead className="bg-dark text-light">
              <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {orders.map((order) => (
                <tr key={order._id}>
                  <td>{order.products[0].productName}</td>
                  <td>{order.products[0].productId}</td>
                  <td>{order.products[0].subTotal}</td>
                  <td>{order.status}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
}

