import { useState, useEffect, useContext } from 'react'; 
import UserContext from '../UserContext';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useParams, useNavigate, Link } from 'react-router-dom';

export default function ProductView() {

  const { user } = useContext(UserContext);

  const navigate = useNavigate();
  const { productId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const updateQuantity = (increment) => {
    if (increment) {
      setQuantity(quantity + 1);
    } else if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  const calculateTotal = () => {
    return price * quantity;
  };

  const orderCreate = (productId) => {
    const subTotal = calculateTotal();
    const totalAmount = subTotal;
    const order = {
      product_id: productId,
      name: name,
      quantity: quantity,
      sub_total: subTotal,
      total_amount: totalAmount
    };
    fetch(`${process.env.REACT_APP_API_URL}/orders/create`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(order)
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Order Confirmed!",
            icon: "success",
            text: "Thank you for purchasing."
          });

          navigate("/products");

        } else {
          Swal.fire({
            title: "Uh! Oh! Something went wrong",
            icon: "error",
            text: "Please try again."
          });
        }
      });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

  return (
    <Container className="justify-content-center p-5 mt-5">
      <Col lg={{ span: 3, offset: 4 }}>
        <Card className="my-card">
          <Card.Body className="text-center my-card">
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>PhP {price}</Card.Text>
            <Row>
              <Col>
                <Button variant="outline-secondary" onClick={() => updateQuantity(false)}>-</Button>
              </Col>
              <Col>
                <Card.Text>{quantity}</Card.Text>
              </Col>
              <Col>
                <Button variant="outline-secondary" onClick={() => updateQuantity(true)}>+</Button>
              </Col>
            </Row>
            <Card.Subtitle>Total Amount:</Card.Subtitle>
            <Card.Text>PhP {calculateTotal()}</Card.Text>
            {
              user.id !== null && !user.isAdmin ?
                <Button variant="primary" onClick={() => orderCreate(productId)}>Order Now!</Button>
                :
                user.isAdmin && <div>You're an admin dummy !</div> ||
                <Button className="btn btn-danger" as={Link} to="/login">Log in to Order</Button>
            }
					</Card.Body>
				</Card>
			</Col>
		</Container>

	)
}