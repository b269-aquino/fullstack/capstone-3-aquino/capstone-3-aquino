import {useContext} from 'react';
import {Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default function AppNavbar() 
{

  const {user} = useContext(UserContext)

  return (
    <Navbar className="my-navbar px-5" expand="lg">
        <Navbar.Brand as={Link} to="/">Lucky Flower</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="justify-content-end">
          {user.isAdmin ? (
            <>
              <Nav.Link as={NavLink} to="/admin-page">Admin Control</Nav.Link>
              <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
            </>
          ) : (
            <>
              <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
              {user.id !== null ? (
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                  <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                </>
              )}
            </>
          )}
        </Nav>
        </Navbar.Collapse>
    </Navbar>
  );
}