import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

import { Button, Row, Col, Card, Container } from 'react-bootstrap';


export default function ProductCard({product}) {
    const { name, description, price, _id } = product;

    return (
        <Container>
                <Col lg={{ span: 6, offset: 3 } } className="p-3">
                    <Card className="my-card">
                        <Card.Body className="text-center my-card">
                            <Card.Title><h4>{name}</h4></Card.Title>
                            <Card.Subtitle>Description</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
                        </Card.Body>
                    </Card>
                </Col>
        </Container>      
    )
}
