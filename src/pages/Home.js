import Banner from '../components/Banner';


export default function Home() {

	const data = {
		title: "Lucky Flower",
		content: "A Lovely BOQUET of flowers for your LUCKY ONE ! ",
		destination: "/products",
		label: "Shop now!"
	}


	return (
		<>
		<Banner data={data} /> 	
		</>
	)
}

