import { useState, useEffect } from 'react';
import { Table, Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(data);
      });
  }, []);

  function updateProductStatus(productId, isActive) {
    const requestBody = { isActive: isActive };
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(requestBody),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts((prevProducts) =>
          prevProducts.map((product) =>
            product._id === productId ? { ...product, isActive: data.isActive } : product
          )
        );
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }

  return (
    <Container className="text-center">
      <Row>
        <Col className="p-5">
          <h1>Hello Admin!</h1>
          <h3>Admin Dashboard</h3>
          <Button variant="primary px-3 mx-1" as={Link} to={"/add-product"} >Add New Product</Button>
          <Button variant="success px-3 mx-1" as={Link} to={"/user-orders"} >Show Users Orders</Button>
        </Col>
      </Row>
      <Row className="justify-content-center align-items-center">
        <Col>
          <Table striped bordered hover>
            <thead className="bg-dark text-light">
              <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {products.map((product) => (
                <tr key={product._id} className={product.isActive ? '' : 'bg-light'}>
                  <td>{product.name}</td>
                  <td>{product.description}</td>
                  <td>{product.price}</td>
                  <td>{product.isActive ? 'Active' : 'Inactive'}</td>
                  <td>
                    <Button variant="primary mx-1" as={Link} to={`/update-product/${product._id}`}>Update</Button>
                    {product.isActive ? (
                      <Button variant="danger mx-1" onClick={() => updateProductStatus(product._id, false)}>Disable</Button>
                    ) : (
                      <Button variant="success mx-1" onClick={() => updateProductStatus(product._id, true)}>Enable</Button>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
}
